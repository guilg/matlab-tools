function [] = manualerrbar(x, y, varargin)
% manualerrbar(x, y, varargin)
%
% Add vertical bars to current axes.
%
% INPUTS :
% ------
% x : x location of the n vertical bars
% y : n x 2 array with the [ymin; ymax] values of the n bars.
% Everything else is passed to the plot function.

ax = gca;

for id = 1:numel(x)
    
    plot(ax, [x(id), x(id)], [y(id, 1), y(id, 2)], varargin{:});
    
end