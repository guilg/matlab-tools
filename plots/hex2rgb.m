function rgb = hex2rgb(hex)
% rgb = HEX2RGB(hex);
%
% Convert hexadecimal color code to RGB (between 0 and 1).

% --- Check input
p = inputParser;
p.addRequired('hex', @(x) isstring(x)||ischar(x));
p.parse(hex);

hex = p.Results.hex;

if ~strcmp(hex(1), '#')
    hex = ['#', hex];
end

f = figure('Visible', 'off'); ax = axes(f, 'Visible', 'off');
p = plot(ax, NaN, 'Color', hex, 'Visible', 'off');
rgb = p.Color;

delete(f);

end