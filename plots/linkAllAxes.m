function [] = linkAllAxes()
% LINKALLAXES();
%
% Link all axes from figures currently opened.

H = get(0, 'Children');

axs = [];

for idx_fig = 1:length(H)
    
    axs(end + 1:end + length(H(idx_fig).Children)) = H(idx_fig).Children;

end

linkaxes(axs);