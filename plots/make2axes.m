function [ax1, ax2] = make2axes()
% [ax1, ax2] = MAKE2AXES;
%
% Creates two axes handles on the same figure.

figure;

ax1 = axes; hold(ax1, 'on');
grid(ax1, 'off');

ax2 = axes; hold(ax2, 'on');
ax2.Position = ax1.Position;
ax2.XAxisLocation = 'top';
ax2.YAxisLocation = 'right';
ax2.Color = 'none';
grid(ax2, 'off');