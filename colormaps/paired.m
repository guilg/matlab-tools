function map = paired(m)
%PAIRED Matplotlib colormap
%   PAIRED(M) returns an M-by-3 matrix containing a colormap.
%
%   PAIRED returns a colormap with the same number of colors as the current
%   figure's colormap. If no figure exists, MATLAB uses the length of the
%   default colormap.
%
%   Values are taken from the paired colormap of matplotlib.
%   
%   EXAMPLE
%
%   This example shows how to reset the colormap of the current figure.
%
%       colormap(PAIRED)
%
%   See also AUTUMN, BONE, COLORCUBE, COOL, COPPER, FLAG, GRAY, HOT, HSV,
%   JET, LINES, PINK, PRISM, SPRING, SUMMER, WHITE, WINTER, COLORMAP,
%   RGBPLOT.

if nargin < 1
    f = get(groot,'CurrentFigure');
    if isempty(f)
        m = size(get(groot,'DefaultFigureColormap'),1);
    else
        m = size(f.Colormap,1);
    end
end

paired_data = [166,206,227;31,120,180;178,223,138;51,160,44;251,154,153;227,26,28;253,191,111;255,127,0;202,178,214;106,61,154;255,255,153;177,89,40];
paired_data = paired_data./255;

map = paired_data(rem(0:m-1,size(paired_data,1))+1,:);