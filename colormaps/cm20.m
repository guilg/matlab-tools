function map = cm20(m)
%CM20  Color map with 20 different colors.
%   
% Taken from :
% https://sashat.me/2017/01/11/list-of-20-simple-distinct-colors/
%
%   See also HSV, GRAY, PINK, COOL, BONE, COPPER, FLAG, 
%   COLORMAP, RGBPLOT.

if nargin < 1
   f = get(groot,'CurrentFigure');
   if isempty(f)
      m = size(get(groot,'DefaultFigureColormap'),1);
   else
      m = size(f.Colormap,1);
   end
end

c = [230,25,75;60,180,75;255,225,25;0,130,200;245,130,48;145,30,180;70,240,240;240,50,230;210,245,60;250,190,212;0,128,128;220,190,255;170,110,40;255,250,200;128,0,0;170,255,195;128,128,0;255,215,180;0,0,128;128,128,128;255,255,255;0,0,0];
c = c./255;
map = c(rem(0:m-1,size(c,1))+1,:);