function map = fire(m)
%FIRE Colorcet colormap
%   FIRE(M) returns an M-by-3 matrix containing a colormap.
%
%   FIRE returns a colormap with the same number of colors as the current
%   figure's colormap. If no figure exists, MATLAB uses the length of the
%   default colormap.
%
%   Values are taken from the colorcet's fire colormap included as csv
%   file (https://raw.githubusercontent.com/holoviz/colorcet).
%   
%   EXAMPLE
%
%   This example shows how to reset the colormap of the current figure.
%
%       colormap(fire)
%
%   See also AUTUMN, BONE, COLORCUBE, COOL, COPPER, FLAG, GRAY, HOT, HSV,
%   JET, LINES, PINK, PRISM, SPRING, SUMMER, WHITE, WINTER, COLORMAP,
%   RGBPLOT.

if nargin < 1
    f = get(groot,'CurrentFigure');
    if isempty(f)
        m = size(get(groot,'DefaultFigureColormap'),1);
    else
        m = size(f.Colormap,1);
    end
end

fire_data = load('/home/guillaume/Documents/MATLAB/Tools/colormaps/linear_kryw_0-100_c71_n256.csv');

P = size(fire_data,1);
map = interp1(1:size(fire_data,1), fire_data, linspace(1,P,m), 'linear');