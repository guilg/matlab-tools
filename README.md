# Matlab Tools
This is a set of small tools to get some things done under MATLAB, such as beautiful plot (boxplot, shaded error bars), some statistical stuff (pdf, cdf, batch significance tests) and some extra.
The MLab folder contains the last version of MLab, a MATLAB plugin created by Raphaël Candelier (http://candelier.fr/MLab/) and has its own licence (GNU GPL v3).