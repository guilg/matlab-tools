function X_shuffled = shuffleArray(X, dim)
% This function shuffles elements of X.
%
% INPUTS :
% ------
% X : 1D or 2D array.
% dim : dimension along which the shuffling is performed (default = 1)
%
% OUTPUTS :
% -------
% X_shuffled : shuffled array with values in X.

% --- Default values
if ~exist('dim', 'var')
    dim = find(size(X) > 1);
end

if dim == 1
    X_shuffled = X(randperm(size(X, 1)), :);
elseif dim == 2
    X_shuffled = X(:, randperm(size(X, 2)));
end