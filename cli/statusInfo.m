function [] = statusInfo(iteration, n_iterations, bar_size)
% Shows in command window a progress bar.

% INPUTS :
% ------
% iteration : int, current iteration
% n_iterations : int, total number of iterations.
% bar_size : int, size of progress bar, in command window char (default : 50).

% --- Default values
if ~exist('bar_size', 'var')
    bar_size = 25;
end

% --- Initialise user message
usr_msg = ['[' repmat(sprintf(' '), 1, bar_size) ']'];
if iteration == 1
    fprintf(usr_msg);
end

% Check if a new step is displayed
if mod(iteration, fix(n_iterations/bar_size)) == 0
    fprintf(repmat(sprintf('\b'), 1, numel(usr_msg)));  % delete previous message
    usr_msg(2:fix(iteration*bar_size/n_iterations)+1) = '=';
    fprintf(usr_msg);
end
end