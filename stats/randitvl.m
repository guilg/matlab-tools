function r = randitvl(a, b)
% r = RANDINT(a, b);
% Generate random number in the interval ]a; b[.
%
% INPUTS :
% ------
% a, b : two number defining an interval, b > a.
%
% RETURNS :
% -------
% r : number in open interval (a; b), or empty if b <= a

if a >= b
    r = [];
else
    r = (b-a).*rand() + a;
end

end