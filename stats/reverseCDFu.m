function value = reverseCDFu(xiu, cdfu, rd)
% value = REVERSECDF(xi, cdf);
% Pick a random value from possible values, xiu, based on the cumulative
% distribution function, cdfu. xiu and cdfu should be unique !
%
% INPUTS :
% ------
% xi : possible values.
% cdf : cumulative distribution function of xi values, must be same size as
% xi.
% rd : impose the value of the cdf to inverse (optional).

% RETURNS :
% -------
% value : random value from xi.

if ~exist('rd', 'var')
    a = min(cdfu);
    b = max(cdfu);
    rd = randitvl(a, b);                    % pick value of CDF
end

value = gather(interp1(cdfu, xiu, rd, 'linear', 'extrap'));   % get its corresponding value

end