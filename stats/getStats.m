function [me, md, sd, sk, ku] = getStats(X)
% Returns mean, median and standart deviation of X. X is linearized.

me = nanmean(X(:));
md = nanmedian(X(:));
sd = nanstd(X(:));
sk = skewness(X(:));
ku = kurtosis(X(:));