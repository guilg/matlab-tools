root = "/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis_Old";

folders = dir(root);
folders(1:2) = [];

for f = 1:length(folders)
    
    if folders(f).isdir
        
        old = [folders(f).folder filesep folders(f).name];
        new = [folders(f).folder filesep folders(f).name(1:end-12)];
        command = ['mv "' old '" "' new '"'];
        unix(command);
    end
end