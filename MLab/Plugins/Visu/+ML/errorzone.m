function out = errorzone(varargin)
%ML.errorzone Error zone plot

% --- Inputs
in = ML.Input;
in.x = @isnumeric;
in.y = @isnumeric;
in.e = @isnumeric;
in.FaceColor('none') = @(x) ischar(x) || isnumeric(x);
in.EdgeColor('none') = @(x) ischar(x) || isnumeric(x);
in.FaceAlpha(0.1) = @isnumeric;
in.smooth(1) = @isnumeric;
[in, unmin] = +in;

% --- Checks


% --- Display
% X = [in.x(:) ; flipud(in.x(:))];
% Y = [in.y(:)-in.e(:) ; flipud(in.y(:)+in.e(:))];

X = [smooth(in.x(:), in.smooth) ; smooth(flipud(in.x(:)), in.smooth)];
Y = [smooth(in.y(:)-in.e(:), in.smooth) ; smooth(flipud(in.y(:)+in.e(:)), in.smooth)];

out = patch(X, Y, in.FaceColor, 'EdgeColor', in.EdgeColor, ...
    'FaceAlpha', in.FaceAlpha, unmin{:});