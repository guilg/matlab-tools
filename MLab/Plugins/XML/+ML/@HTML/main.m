function out = main(this, varargin)
%ML.HTML.main Add a <main> element to the HTML object.
%   ML.HTML.MAIN() add a <main> element to the HTML object.
%
%   ML.HTML.MAIN(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.MAIN(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.MAIN(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'main', 'options', notin);

% --- Output
if nargout
    out = id;
end