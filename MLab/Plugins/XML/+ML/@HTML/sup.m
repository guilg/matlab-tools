function [out1, out2] = sup(this, varargin)
%ML.HTML.sup Add a <sup> element to the HTML object.
%   ML.HTML.SUP() add a <sup> element to the HTML object.
%
%   ML.HTML.SUP(TEXT) specifies the text content of the element.
%
%   ML.HTML.SUP(POSITION, TEXT) also specifies the position of the element.
%
%   ML.HTML.SUP(..., 'opt', OPT, ...) specifies the element's options.
%
%   [ID, ID_TXT] = ML.HTML.SUP(...) returns the identifiers of the new 
%   element an the text content.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('text', @ischar);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'sup', 'options', notin);
id_txt = this.add(id, 'text', in.text);

% --- Outputs
if nargout
    out1 = id;
    out2 = id_txt;
end