function out = input(this, varargin)
%ML.HTML.input Add an <input> element to the HTML object.
%   ML.HTML.INPUT() add an <input> element to the HTML object.
%
%   ML.HTML.INPUT(TYPE) specifies the type of the element.
%
%   ML.HTML.INPUT(POSITION, TYPE) also specifies the position of the 
%   element.
%
%   ML.HTML.INPUT(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.INPUT(...) returns the identifiers of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('type', @ischar);
[in, notin] = +in;

% =========================================================================

notin = ['type' ; in.type; notin];
id = this.add(in.position, 'utag', 'input', 'options', notin);

% --- Outputs
if nargout
    out = id;
end