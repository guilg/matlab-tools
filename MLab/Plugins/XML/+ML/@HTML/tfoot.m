function out = tfoot(this, varargin)
%ML.HTML.tfoot Add a <tfoot> element to the HTML object.
%   ML.HTML.TFOOT() add a <tfoot> element to the HTML object.
%
%   ML.HTML.TFOOT(POSITION) specifies the position of the element. 
%   The default position is given by the object property 'parent'.
%
%   ML.HTML.TFOOT(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.TFOOT(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'tfoot', 'options', notin);

% --- Output
if nargout
    out = id;
end