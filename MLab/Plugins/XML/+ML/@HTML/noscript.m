function out = noscript(this, varargin)
%ML.HTML.noscript Add a <noscript> element to the HTML object.
%   ML.HTML.NOSCRIPT() add a <noscript> element to the HTML object.
%
%   ML.HTML.NOSCRIPT(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.NOSCRIPT(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.NOSCRIPT(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'noscript', 'options', notin);

% --- Output
if nargout
    out = id;
end