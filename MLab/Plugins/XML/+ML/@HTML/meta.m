function out = meta(this, varargin)
%ML.HTML.meta Add an <meta> element to the HTML object.
%   ML.HTML.META() add an <meta> element to the HTML object.
%
%   ML.HTML.META(POSITION) specifies the position of the element.
%
%   ML.HTML.META(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.META(...) returns the identifiers of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'utag', 'meta', 'options', notin);

% --- Outputs
if nargout
    out = id;
end