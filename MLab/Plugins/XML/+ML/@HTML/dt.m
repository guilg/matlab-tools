function [out1, out2] = dt(this, varargin)
%ML.HTML.dt Add a <dt> element to the HTML object.
%   ML.HTML.DT() add a <dt> element to the HTML object.
%
%   ML.HTML.DT(TEXT) specifies the text content of the element.
%
%   ML.HTML.DT(POSITION, TEXT) also specifies the position of the element.
%
%   ML.HTML.DT(..., 'opt', OPT, ...) specifies the element's options.
%
%   [ID, ID_TXT] = ML.HTML.DT(...) returns the identifiers of the new 
%   element an the text content.
%
%   See also ML.HTML, ML.HTML.dl, ML.HTML.dd.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('text', @ischar);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'dt', 'options', notin);
id_txt = this.add(id, 'text', in.text);

% --- Outputs
if nargout
    out1 = id;
    out2 = id_txt;
end