function out = table(this, varargin)
%ML.HTML.table Add a <table> element to the HTML object.
%   ML.HTML.TABLE() add a <table> element to the HTML object.
%
%   ML.HTML.TABLE(POSITION) specifies the position of the element. 
%   The default position is given by the object property 'parent'.
%
%   ML.HTML.TABLE(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.TABLE(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'table', 'options', notin);

% --- Output
if nargout
    out = id;
end