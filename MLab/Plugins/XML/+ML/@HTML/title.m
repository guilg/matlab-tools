function id = title(this, varargin)
%ML.HTML.title Add a <title> element to the HTML object.
%   ML.HTML.TITLE(T) add a <title> element to the HTML object containing
%   the T string. The parent of the title element is the HTML header.
%
%   [ID, ID_TXT] = ML.HTML.TITLE(...) returns the identifiers of the title
%   and title string elements.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addRequired('title', @ischar);
in = +in;

% =========================================================================

id = this.add(this.head, 'tag', 'title');
this.add(id, 'text', in.title);