function [out1, out2] = summary(this, varargin)
%ML.HTML.summary Add a <summary> element to the HTML object.
%   ML.HTML.SUMMARY() add a <summary> element to the HTML object.
%
%   ML.HTML.SUMMARY(TEXT) specifies the text content of the element.
%
%   ML.HTML.SUMMARY(POSITION, TEXT) also specifies the position of the element.
%
%   ML.HTML.SUMMARY(..., 'opt', OPT, ...) specifies the element's options.
%
%   [ID, ID_TXT] = ML.HTML.SUMMARY(...) returns the identifiers of the new 
%   element an the text content.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('text', @ischar);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'summary', 'options', notin);
id_txt = this.add(id, 'text', in.text);

% --- Outputs
if nargout
    out1 = id;
    out2 = id_txt;
end