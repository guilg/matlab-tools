function out = h3(this, varargin)
%ML.HTML.h3 Add a <h3> element to the HTML object.
%   ML.HTML.H3() add a <h3> element to the HTML object.
%
%   ML.HTML.H3(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.H3(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.H3(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'h3', 'options', notin);

% --- Output
if nargout
    out = id;
end