function out = h1(this, varargin)
%ML.HTML.h1 Add a <h1> element to the HTML object.
%   ML.HTML.H1() add a <h1> element to the HTML object.
%
%   ML.HTML.H1(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.H1(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.H1(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'h1', 'options', notin);

% --- Output
if nargout
    out = id;
end