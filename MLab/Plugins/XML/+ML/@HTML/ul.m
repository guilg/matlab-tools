function out = ul(this, varargin)
%ML.HTML.ul Add a <ul> element to the HTML object.
%   ML.HTML.UL() add a <ul> element to the HTML object.
%
%   ML.HTML.UL(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.UL(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.UL(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'ul', 'options', notin);

% --- Output
if nargout
    out = id;
end