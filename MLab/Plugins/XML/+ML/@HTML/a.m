function [out1, out2] = a(this, varargin)
%ML.HTML.a Add a <a> element to the HTML object.
%   ML.HTML.A() add a <a> element to the HTML object.
%
%   ML.HTML.A(HREF, TEXT) specifies the href property and the text content 
%   of the element.
%
%   ML.HTML.A(POSITION, HREF, TEXT) also specifies the position of the 
%   element.
%
%   ML.HTML.A(..., 'opt', OPT, ...) specifies the element's options.
%
%   [ID, ID_TXT] = ML.HTML.A(...) returns the identifiers of the new 
%   element an the text content.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('href', @ischar);
in.addRequired('text', @ischar);
[in, notin] = +in;

% =========================================================================

notin = ['href' ; in.href ; notin];
id = this.add(in.position, 'tag', 'a', 'options', notin);
id_txt = this.add(id, 'text', in.text);

% --- Outputs
if nargout
    out1 = id;
    out2 = id_txt;
end