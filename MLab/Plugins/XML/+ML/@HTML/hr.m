function out = hr(this, varargin)
%ML.HTML.hr Add a <hr> element to the HTML object.
%   ML.HTML.HR() add a <hr> element to the HTML object.
%
%   ML.HTML.HR(POSITION) specifies the position of the element. The default
%   position is given by the HTML object property 'parent'.
%
%   ID = ML.HTML.HR(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
in = +in;

% =========================================================================

id = this.add(in.position, 'utag', 'hr');

% --- Output
if nargout
    out = id;
end