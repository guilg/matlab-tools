function tag = html(this, varargin)
%ML.GUI.html HTML widget shortcut
%
%   See also: ML.GUI.

% === Inputs ==============================================================

in = ML.Input;

% --- Required
if this.grid_layout
    in.row = @isnumeric;
    in.col = @isnumeric;
else
    in.pos = @isnumeric;
end
in.html = @ischar;

% --- Param / value pairs
in.Tag('') = @ischar;
in.tab(this.current_tab) = @isnumeric;
if this.grid_layout
    in.rowspan(1) = @isnumeric;
    in.colspan(1) = @isnumeric;
    in.halign('center') = @(x) ismember(x, {'left', 'center', 'right'});
    in.valign('center') = @(x) ismember(x, {'bottom', 'center', 'top'});
end

[in, notin] = +in;

% =========================================================================

% --- Parameters for the figure widget 
in.params = {'Style', 'html'};
in.params = [in.params {'Tag', in.Tag}];
in.params = [in.params {'html', in.html}];
in.params = [in.params notin'];

% --- Define figure widget
tmp = this.def_widget(in);

% --- Output
if nargout
    tag = tmp;
end