function tag = pushbutton(this, varargin)
%ML.GUI.txt Text shortcut
%   ML.HUI.TEXT([ROW COL], TXT) adds a text widget in the cell [ROW COL]
%   of the current grid.
%
%   ML.HUI.TEXT(..., PARAM, VALUE) defines PARAM / VALUE couples. Valid
%   options are those that a <p> tag can handle. VALUE can be either a 
%   string or a numeric value.
%
%   See also: ML.HUI.

% === Inputs ==============================================================

in = ML.Input;

% --- Required
if this.grid_layout
    in.row = @isnumeric;
    in.col = @isnumeric;
else
    in.pos = @isnumeric;
end
in.String = @ischar;
in.Callback = @ML.isfunction_handle;

% --- Param / value pairs
in.Tag('') = @ischar;
in.tab(this.current_tab) = @isnumeric;
if this.grid_layout
    in.rowspan(1) = @isnumeric;
    in.colspan(1) = @isnumeric;
    in.halign('center') = @(x) ismember(x, {'left', 'center', 'right'});
    in.valign('center') = @(x) ismember(x, {'bottom', 'center', 'top'});
end

[in, notin] = +in;

% =========================================================================

% --- Parameters for the figure widget 
in.params = {'Style', 'pushbutton'};
in.params = [in.params {'Tag', in.Tag}];
in.params = [in.params {'String', in.String}];
in.params = [in.params {'Callback', @(hObject, callbackdata) this.(func2str(in.Callback))}];
in.params = [in.params {'BackgroundColor', 'w'}];
in.params = [in.params notin'];

% Img = imresize(imread('/home/ljp/Bureau/Steren_Glossy_Rectangle.png'), pos([4 3]));
% set(h, 'CData', Img);

% --- Remove useless fields
in = rmfield(in, 'Tag');
in = rmfield(in, 'String');
in = rmfield(in, 'Callback');

% --- Define figure widget
tmp = this.def_widget(in);

% --- Output
if nargout
    tag = tmp;
end