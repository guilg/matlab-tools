function out = new_tab(this, varargin)
%ML.GUI.new_tab New tab method
%   TAB = ML.GUI.NEW_TAB() creates a new tab. The output TAB is the tab
%   identifier.
%
%   See also: ML.HUI.

% === Input ===============================================================

in = ML.Input(varargin{:});
in.addRequired('name', @ischar);
in = +in;

% =========================================================================

if numel(this.tabs)==1 && isempty(this.tabs(1).name)
    n = 1;
else
    n = numel(this.tabs)+1;
end

this.tabs(n).name = in.name;
this.current_tab = n;
this.display_tab = n;

% --- Output
if nargout
    out = n;
end