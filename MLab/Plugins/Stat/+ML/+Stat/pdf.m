function P = pdf(bin, val, varargin)
%STAT.PDF Probability distribution function
%*  P = STAT.PDF(BINS, VALUES) computes the pdf of the vector VALUES, over 
%   the  bins edges BINS. BINS can be either a n-by-1 binning vector or a 
%   single value representing the number of bins. It returns a structure P
%   containing the following fields: 'bin', 'hist', 'pdf'.
%
%*  STAT.PDF(..., 'binning', 'log') uses a logarithmic binning. the default
%   values for option 'binning' is 'lin'.
%
%*  See also: Stat.cpdf, Stat.angpdf.

% === Input variables =====================================================

in = inputParser;
in.addRequired('bin', @isnumeric);
in.addRequired('val', @isnumeric);
in.addParamValue('binning', 'lin', @(x) strcmp(x,'lin') | strcmp(x, 'log'));

in.parse(bin, val, varargin{:});
in = in.Results;

% =========================================================================

% --- Automatic binning mode

% Default mode
if (in.bin<=0 | isnan(in.bin)), in.bin = nextprime(numel(in.val)/20); end

% Single value
if numel(in.bin)==1
    switch in.binning
        case 'lin'
            in.bin = linspace(nanmin(in.val), nanmax(in.val), in.bin+1);
        case 'log'
            if nanmin(in.val)<=0
                warning('STAT.PDF:NegativeValues', 'Some values are below or equal to zero, they are skipped.');
                in.val = in.val(in.val>0);
            end
            in.bin = logspace(log10(nanmin(in.val)), log10(nanmax(in.val)), in.bin+1);
    end
end

% Precautions
in.bin = squeeze(sort(in.bin))';
in.val = squeeze(in.val)';

% --- Checks
if ~isprime(numel(in.bin)-1)
    % fprintf('Warning: The number of bins (%i) is not a prime number.\n', numel(in.bin)-1);
end

% --- Process

% Histogram
h = histc(in.val, in.bin);
h = [h(1:end-2) sum(h(end-1:end))];
k = h./diff(in.bin');

% Binning
bin = [];
switch in.binning
    case 'lin'
        for i = 1:numel(in.bin)-1
            bin(i) = (in.bin(i) + in.bin(i+1))/2;
        end

    case 'log'
        for i = 1:numel(in.bin)-1
            bin(i) = sqrt(in.bin(i)*in.bin(i+1));
        end
end

% Integral
z = trapz(bin, k);

% --- Output
P = struct('bin', bin', ...
           'hist', h', ...
           'pdf', (k./z)');
