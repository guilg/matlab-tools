function out = replace(this, uid, text, tag)
%ML.THF.replace Replace tagged text based on uid identifier
%
%   See alos: ML.THF.add.

% --- String to replace

tmp = regexp(this.content, ['(<[^<>]*id="' num2str(uid) '">.*</[^<>]*id="' num2str(uid) '">)'], 'tokens');
str = tmp{1}{1};

% --- Default tag
if ~exist('tag', 'var'), tag = 'p'; end

% --- Specific tags
switch tag
    case 'item'
        text = ['<li>' text '</li>'];
        tag = 'ul';
end

% --- Update the content
this.content = strrep(this.content, str, sprintf('\n<%s id="%s">%s</%s id="%s">', tag, num2str(uid), text, tag,  num2str(uid)));

% --- Output
if nargout
    out = this.content;
end