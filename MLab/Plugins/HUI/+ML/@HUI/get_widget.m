function W = get_widget(this, varargin)
%ML.HUI.get_widget
%
%   See also: ML.HUI.

% === Inputs ==============================================================

in = ML.Input;

in.src = @(x) isstruct(x) || ischar(x);

in = +in;

% =========================================================================

% --- Default tab
if ischar(in.src)
    in.src = struct('tab', this.display_tab, 'tag', in.src);
end

% --- Get widget
I = cellfun(@(x) strcmp(x.tag, in.src.tag), this.tabs(in.src.tab).widgets);
if any(I)
    W = this.tabs(in.src.tab).widgets{I};
else
    W = NaN;
end