function out = scatter(this, varargin)
%GEOM.POINTS.SCATTER scatter plot of the points
%*  GEOM.POINTS.SCATTER(..., 'Property', PROPERTY) has a similar usage than
%   the Matlab SCATTER function, except that the two arguments X and Y are
%   automatically set to the points positions.
%
%[  Note: this function works in dimensions 1, 2 and 3. In dimension 3 the
%   Matlab function SCATTER3 is employed.  ]
%
%[  Note: the 'axes_handle' option cannot be specified.  ]
%
%*  See also: Geom.Points, scatter, scatter3.

switch this.dim
    
    case 1
        
        h = scatter(this.pos(:,1), zeros(this.N,1), varargin{:});
        
    case 2
        
        h = scatter(this.pos(:,1), this.pos(:,2), varargin{:});
        
    case 3
        
        h = scatter3(this.pos(:,1), this.pos(:,2), this.pos(:,3), varargin{:});
        
    otherwise
   
        error('POINTS.SCATTER:WrongDim', 'Scatter plots cannot be generated for this dimension');
end

if nargout, out = h; end