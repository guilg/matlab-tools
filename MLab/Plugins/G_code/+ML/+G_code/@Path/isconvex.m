function out = isconvex(P)
%ISCONVEX Checks 2D polygon's convexity
%*  OUT = ISCONVEX(P) checks if polygon P is convex. P should be a n-by-2 
%   array, represeting a n point polygon with vertices [x y].
%
%*  See also: istrigo.

U = [P(2:end,:) ; P(1,:)]-P;
V = [U(2:end,:) ; U(1,:)];

out = abs(sum(sign(U(:,1).*V(:,2) - U(:,2).*V(:,1))))==size(P,1);