function rect(this, pos, s, z, varargin)
%G_CODE::RECT Mill a rectangle
%*  RECT(POS, SIZE, Z) where POS is (x,y), SIZE is (w,h) and Z is the
%   depth.
%
%*  RECT(..., 'key', value) specifies options. Keys can be:
%   * 'tool' (this.tool):   The tool to use
%   * 'z_step' (tool.diam): The z-step size
%   * 'pref_axis' : the prefered axis ('x' or 'y', default is 'x')
%
%
%*  See also: G_CODE, G_CODE::hole, G_CODE::traj.

% === Input variables =====================================================

in = inputParser;
in.addRequired('pos', @isnumeric);
in.addRequired('s', @isnumeric);
in.addRequired('z', @isnumeric);
in.addParamValue('z_step', this.tools(this.tool).diam, @isnumeric);
in.addParamValue('z_start', 0, @isnumeric);
in.addParamValue('tool', this.tool, @isnumeric);
in.addParamValue('pref_axis', 'x', @(x) strcmp(x,'x') | strcmp(x,'y'));
in.addParamValue('xy_step', NaN, @isnumeric);

in.parse(pos, s, z, varargin{:});
in = in.Results;

% =========================================================================

% -- Definitions ----------------------------------------
tool = this.tools(in.tool);
r = tool.diam/2;
x = pos(1);
y = pos(2);
dx = s(1);
dy = s(2);

% -- Checks --------------------------------------------

if (dx<2*r)
    disp(['G_CODE::RECT hor. dim. (' num2str(dx) 'mm) too low for '...
        'this tool (diam ' num2str(2*r) 'mm).  Aborting.']);
    return;
end

if (dy<2*r)
    disp(['G_CODE::RECT vert. dim. (' num2str(dy) 'mm) too low for '...
        'this tool (diam ' num2str(2*r) 'mm).  Aborting.']);
    return;
end


% -- Trajectory ----------------------------------------

% Position
T = [x+r y+r z];

% -- Outer rectangle
T(end+1, :) = [x+r y+dy-r z];
T(end+1, :) = [x+dx-r y+dy-r z];
T(end+1, :) = [x+dx-r y+r z];
T(end+1, :) = [x+r y+r z];

% If the rectangle isn't already done, snake around
if (dy>4*r) && (dx>4*r)
    
    switch in.pref_axis
        
        case 'x'
            
            if isnan(in.xy_step)
                a = (dy-4*r);
                N_p = ceil(a/r);
                in.xy_step = a/N_p;
            else
                N_p = floor((dy-3*r)/in.xy_step);
            end
            s_p = (r-in.xy_step)/2;
            
            y0 = y+r+s_p;           % Reference positions
            x0 = x+3/2*r;
            x1 = x+dx-3/2*r;
            
            for i = 1:N_p+1
                
                if mod(i,2)
                    T(end+1, :) = [x0 y0+i*in.xy_step z];
                    T(end+1, :) = [x1 y0+i*in.xy_step z];
                else
                    T(end+1, :) = [x1 y0+i*in.xy_step z];
                    T(end+1, :) = [x0 y0+i*in.xy_step z];
                end
                
            end
            
        case 'y'
            
            if isnan(in.xy_step)
                a = (dx-4*r);
                N_p = ceil(a/r);
                in.xy_step = a/N_p;
            else
                N_p = floor((dx-3*r)/in.xy_step);
            end
            s_p = (r-in.xy_step)/2;
            
            x0 = x+r+s_p;           % Reference positions
            y0 = y+3/2*r;
            y1 = y+dy-3/2*r;
            
            for i = 1:N_p+1
                
                if mod(i,2)
                    T(end+1, :) = [x0+i*in.xy_step y0 z];
                    T(end+1, :) = [x0+i*in.xy_step y1 z];
                else
                    T(end+1, :) = [x0+i*in.xy_step y1 z];
                    T(end+1, :) = [x0+i*in.xy_step y0 z];
                end
                
            end
            
    end
    
end

% No curvature
T(:,4:6) = NaN;

% --- Append trajectory
this.traj(T, 'tool', in.tool, 'z_step', in.z_step, 'z_start', in.z_start);