function [x, y, w, h] = xywh(this)
%ML.Rect
%   [X, Y, W, H] = ML.XYWH() returns the rectangle coordinates as a 
%   position and a couple width-height.
%
%   See also ML.Rect.

% --- Output
x = this.x1;
y = this.y1;
w = this.x2 - this.x1;
h = this.y2 - this.y1;

if nargout==1
    x = [x y w h];
end
    

    