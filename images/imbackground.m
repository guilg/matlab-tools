function [B, S] = imbackground(img, mdl)
%imbackground Get background mean value and standard deviation
%   B = IMBACKGROUND(IMG) Returns the mean value of the background in
%   the image, with a gaussian fit.
%
%   B = IMBACKGROUND(IMG, MDL) specifies the model to use for the fit. Any
%   fit model or string suitable to the FIT function should be accepted but
%   for now only 'Gauss1' and 'Gauss2' are handled.
%
%   [B, S] = IMBACKGROUND(IMG) also returns the standard deviation.
%
%   Adapted from R. Candelier

% --- Check input
if ~exist('mdl', 'var')
    mdl = 'Gauss1';
end

% --- Cumulated PDF
[f, bin] = ecdf(img(:));

% --- Differentiate cPDF (in semilog mode)
b = (bin(1:end-1)+bin(2:end))/2;
x = log(b);
d = diff(f);

I = isfinite(x);
x = x(I);
d = d(I);

% --- Focus on the dark values
[~, m] = max(d);
s = d(1:m);
x = x(1:m);
I = x>0;

% --- Fit on actual values
x = exp(x(I));
s = s(I);
f = fit(x, s, mdl);

% --- Output
switch mdl
    case 'Gauss1'
        B = f.b1;
        S = f.c1/sqrt(2);
    case 'Gauss2'
        [B, am] = min([f.b1,f.b2]);
        if am == 1
            S = f.c1/sqrt(2);
        elseif am == 2
            S = f.c2/sqrt(2);
        end
    otherwise
        error('Fit method not supported.');
end