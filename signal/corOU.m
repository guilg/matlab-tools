function [acf, lag] = corOU(ousignals, ncorr)
% mv = VAROU(delays, ousignals);
%
% Computes the ACF of columns in ousignals.
%
% INPUTS :
% ------
% ousignals : ntimes x nexp
% ncorr : max lag for ACF
%
% OUTPUT :
% ------
% acf : ntimes x nexp array, with acf
% lag : delay vector

% --- Check input
p = inputParser;
p.addRequired('ousignals', @isnumeric);
p.addRequired('ncorr', @isscalar);
p.parse(ousignals, ncorr);
ousignals = p.Results.ousignals;
ncorr = p.Results.ncorr;

nexp = size(ousignals, 2);

% --- Init.
acf = NaN(ncorr + 1, nexp);

% --- Processing
for idF = 1:nexp
    
    vec = ousignals(:, idF);
    vec = vec - mean(vec);  % center
    
    [r, l] = xcorr(vec, ncorr, 'normalized');
    selected_inds = find(l == 0):find(l == 0) + ncorr;
    acf(:, idF) = r(selected_inds);
    lag = l(selected_inds)';
    
end