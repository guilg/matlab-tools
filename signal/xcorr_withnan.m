function [C, lags] = xcorr_withnan(data, varargin)
% C = XCORR_WITHNAN(data, varargin);
%
% Manually computes autocorrelation function with nans, normalized at 0 lag.
%
% INPUTS :
% ------
% data : Nx1 vector
% nlags (optional) : number of lags. By default this is set according to
% data size.
%
% RETURN :
% ------
% C : autocorrelation function computed with spearman coeff

% --- Check input
in = inputParser;
in.addRequired('data', @isnumeric);
in.addOptional('nlags', NaN, @isnumeric);
in.parse(data, varargin{:});
data = in.Results.data;
nlags = in.Results.nlags;

if isnan(nlags)
    nlags = numel(data) - 2;
end

% --- Processing
C = zeros(1, nlags);
C(1) = 1;

for idx = 2:nlags+1
    C(idx) = corr(data(idx:end), data(1:(end - idx + 1)), 'Type', 'Pearson', 'rows', 'complete');
end

lags = 0:nlags;