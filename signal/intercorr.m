function [Xcorr, lags] = intercorr(x, y)
% This function computes the intercorrelation between all rows of the
% matrix x and the y vector. It computes the correlation in the frequency
% domain with FFT. The results is the same as xcorr but works for huge
% matrix.
%
% INPUTS :
% ------
% x : 2D array. Time series to be cross-correlated with y are on rows.
% y : 1D vector. Each row of x will be cross-correlated with y.
%
% OUTPUTS :
% -------
% Xcorr : 2D array, same number of rows as x, twice - 1 columns as x.
% lags : corresponding lags.

m = size(x, 2);     % Number of time points

% Check inputs
% ------------
if ~ismember(size(y), 1)
    error('y should be a vector.');
end

% Maximum lag
maxlag = m - 1;
n = 2^nextpow2(2*maxlag - 1);

% Fourier domain
X = fft(x, n, 2);
Y = fft(y, n, 2);

% Compute the crosscorrelation in the Fourier domain and go back to time
% domain.
c1 = real(ifft(X.*conj(Y), [], 2));

% Reorder
Xcorr = [c1(:, n - maxlag + (1:maxlag)), c1(:, 1:maxlag+1)];

% Lags vector
lags = -maxlag:maxlag;